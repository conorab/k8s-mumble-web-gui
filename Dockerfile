FROM library/debian:bullseye-slim
# libnice-dev, libssl-dev and clang are depenencies for mumble-web-proxy itself.
RUN apt -y update &&\
 apt -y upgrade &&\
 apt -y install curl sudo git &&\
 curl -sL https://deb.nodesource.com/setup_12.x | sudo bash - &&\
 apt install -y nodejs sudo apache2 &&\
 useradd -m mumble-web-temp -r &&\
 sudo -Hu mumble-web-temp bash -c 'cd ~/ && git clone https://github.com/johni0702/mumble-web && cd mumble-web && npm install' &&\
 rm -r /var/www/html &&\
 mv /home/mumble-web-temp/mumble-web/dist /var/www/html &&\
 chown -R www-data:www-data /var/www/html &&\
 userdel mumble-web-temp &&\
 rm -r /home/mumble-web-temp &&\
 apt -y purge nodejs &&\
 apt -y autoremove
CMD [ "bash", "-c", "/usr/sbin/apache2ctl -k start && sleep 10 && while [ -e /var/run/apache2/apache2.pid ] && [ -e /proc/$(</var/run/apache2/apache2.pid) ]; do sleep 1; done" ]
EXPOSE 80/tcp
